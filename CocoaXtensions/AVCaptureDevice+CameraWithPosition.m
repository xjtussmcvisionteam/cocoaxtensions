//
//  AVCaptureDevice+CameraWithPosition.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/3.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "AVCaptureDevice+CameraWithPosition.h"

@implementation AVCaptureDevice (CameraWithPosition)

+ (AVCaptureDevice *) deviceWithPosition:(AVCaptureDevicePosition)position {
    NSArray * availableCameras = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    if (availableCameras.count == 1) {
        // on MAC OS X, the ONLY front facing camera has no 'position' property.
        return availableCameras[0];
    }
    for (AVCaptureDevice * device in availableCameras) {
        if (device.position == position) {
            return device;
        }
    }
    return nil;
}

@end
