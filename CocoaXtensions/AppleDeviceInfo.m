//
//  AppleDeviceInfo.m
//  CocoaXtensions
//
//  Created by JiangZhping on 2016/9/23.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "AppleDeviceInfo.h"

@implementation AppleDeviceInfo

+ (NSString *) deviceCodeToDeviceSizeProfile:(NSString *)deviceCode {
    static NSDictionary<NSString *, NSString *> * deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        deviceNamesByCode = @{// iOS
                              @"iPhone6,1" :@"iPhone4Inch",
                              @"iPhone6,2" :@"iPhone4Inch",
                              @"iPhone8,4" :@"iPhone4Inch",
                              @"iPhone7,2" :@"iPhone4_7Inch",
                              @"iPhone8,1" :@"iPhone4_7Inch",
                              @"iPhone7,1" :@"iPhone5_5Inch",
                              @"iPhone8,2" :@"iPhone5_5Inch",
                              // TODO : support iPhone 7 and 7+
                              @"iPad4,1"   :@"iPad9_7Inch",
                              @"iPad4,2"   :@"iPad9_7Inch",
                              @"iPad4,3"   :@"iPad9_7Inch",
                              @"iPad4,4"   :@"iPad7_9Inch",
                              @"iPad4,5"   :@"iPad7_9Inch",
                              @"iPad4,6"   :@"iPad7_9Inch",
                              @"iPad4,7"   :@"iPad7_9Inch",
                              @"iPad5,1"   :@"iPad7_9Inch",
                              @"iPad5,2"   :@"iPad7_9Inch",
                              @"iPad5,3"   :@"iPad9_7Inch",
                              @"iPad5,4"   :@"iPad9_7Inch",
                              @"iPad6,7"   :@"iPad12_9Inch",
                              @"iPad6,8"   :@"iPad12_9Inch",
                              @"iPad6,3"   :@"iPad9_7Inch",
                              @"iPad6,4"   :@"iPad9_7Inch",
                              // OSX
                              @"MacBookPro11,5"   :@"MBPR15Inch",
                              @"MacBookPro11,4"   :@"MBPR15Inch",
                              @"MacBookPro11,3"   :@"MBPR15Inch",
                              @"MacBookPro11,2"   :@"MBPR15Inch",
                              @"MacBookPro10,1"   :@"MBPR15Inch",
                              @"MacBookPro12,1"   :@"MBPR13Inch",
                              @"MacBookPro11,1"   :@"MBPR13Inch",
                              @"MacBookPro10,2"   :@"MBPR13Inch",
                              @"MacBookAir7,2"   :@"MBA13Inch",
                              @"MacBookAir6,2"   :@"MBA13Inch",
                              @"MacBookAir5,2"   :@"MBA13Inch",
                              @"MacBookAir4,2"   :@"MBA13Inch",
                              @"MacBookAir7,1"   :@"MBA11Inch",
                              @"MacBookAir6,1"   :@"MBA11Inch",
                              @"MacBookAir5,1"   :@"MBA11Inch",
                              @"MacBookAir4,1"   :@"MBA11Inch",
                              @"MacBook8,1"   :@"MB12Inch",
                              @"MacBook9,1"   :@"MB12Inch",
                              @"MacBook9,1"   :@"MB12Inch",
                              @"iMac17,1"   :@"iMac27Inch5K",
                              @"iMac15,1"   :@"iMac27Inch5K",
                              @"iMac17,1"   :@"iMac27Inch5K",
                              @"iMac16,2"   :@"iMac21Inch4K",
                              @"iMac16,1"   :@"iMac21Inch",
                              @"iMac14,4"   :@"iMac21Inch",
                              @"iMac14,3"   :@"iMac21Inch",
                              @"iMac14,1"   :@"iMac21Inch",
                              @"iMac13,1"   :@"iMac21Inch",
                              @"iMac12,1"   :@"iMac21Inch",
                              @"iMac12,2"   :@"iMac27Inch",
                              @"iMac13,2"   :@"iMac27Inch",
                              @"iMac14,2"   :@"iMac27Inch",
                              };
    }
    return deviceNamesByCode[deviceCode];
}

+ (AppleDeviceInfo *) deviceInfoBySizeProfile:(NSString *)sizeProfileCode {
    static NSDictionary<NSString *, AppleDeviceInfo *> * sizeProfileDict;
    
    static AppleDeviceInfo * mbpr15;
    static AppleDeviceInfo * mbpr13;
    static AppleDeviceInfo * mba13;
    static AppleDeviceInfo * mb12;
    static AppleDeviceInfo * mba11;
    static AppleDeviceInfo * iPhone4Inch;
    static AppleDeviceInfo * iPhone4_7Inch;
    static AppleDeviceInfo * iPhone5_5Inch;
    static AppleDeviceInfo * iPad7_9Inch;
    static AppleDeviceInfo * iPad9_7Inch;
    static AppleDeviceInfo * iPad12_9Inch;
    
    mbpr15 = mbpr15 ? mbpr15 : [AppleDeviceInfo deviceInfoWithResolution:CGSizeMake(2880, 1800) PPI:220.0f EquavalentPoint:CGPointMake(1440, 1845)];
    mbpr13 = mbpr13 ? mbpr13 : [AppleDeviceInfo deviceInfoWithResolution:CGSizeMake(2560, 1600) PPI:227.0f EquavalentPoint:CGPointMake(1280, 1650)];
    mba13 = mba13 ? mba13 : [AppleDeviceInfo deviceInfoWithResolution:CGSizeMake(1440, 900) PPI:128.0f EquavalentPoint:CGPointMake(720, 950)];
    mb12 = mb12 ? mba13 : [AppleDeviceInfo deviceInfoWithResolution:CGSizeMake(2340, 1440) PPI:226.0f EquavalentPoint:CGPointMake(1170, 1485)];
    mba11 = mba11 ? mba11 :[AppleDeviceInfo deviceInfoWithResolution:CGSizeMake(1366, 768) PPI:135.f EquavalentPoint:CGPointMake(683, 805)];
    iPhone4Inch = iPhone4Inch ? iPhone4Inch : [AppleDeviceInfo deviceInfoWithResolution:CGSizeMake(640, 1136) PPI:326.f EquavalentPoint:CGPointMakeWithGLKVector2(GLKVector2MultiplyScalar(GLKVector2Make(29.28f - (58.57f-51.70f)/2 - 0.58f, - (16.72-10.75)), MM_TO_INCH * 326.f))];
    iPhone4_7Inch = iPhone4_7Inch ? iPhone4_7Inch : [AppleDeviceInfo deviceInfoWithResolution:CGSizeMake(750, 1334) PPI:326.f EquavalentPoint:CGPointMakeWithGLKVector2(GLKVector2MultiplyScalar(GLKVector2Make(22.92f - (67.2f-58.49f)/2 - 0.3f, - (7.94+0.3)), MM_TO_INCH * 326.f))];
    iPhone5_5Inch=iPhone5_5Inch ? iPhone5_5Inch : [AppleDeviceInfo deviceInfoWithResolution:CGSizeMake(1080, 1920) PPI:401.f EquavalentPoint:CGPointMakeWithGLKVector2(GLKVector2MultiplyScalar(GLKVector2Make(28.33f - (77.94f-68.36f)/2 - 0.3f, - (18.34-9.68+0.3)), MM_TO_INCH * 401.f))];
    iPad7_9Inch = iPad7_9Inch ? iPad7_9Inch : [AppleDeviceInfo deviceInfoWithResolution:CGSizeMake(1536, 2048) PPI:326.f EquavalentPoint:CGPointMakeWithGLKVector2(GLKVector2MultiplyScalar(GLKVector2Make(22.92f - (67.2f-58.49f)/2 - 0.3f, - (7.94+0.3)), MM_TO_INCH * 326.f))];
    iPad9_7Inch=iPad9_7Inch ? iPad9_7Inch : [AppleDeviceInfo deviceInfoWithResolution:CGSizeMake(1536, 2048) PPI:264.f EquavalentPoint:CGPointMakeWithGLKVector2(GLKVector2MultiplyScalar(GLKVector2Make(84.74f - (169.47f-153.71f)/2, - ((240-203.11)/2-11.07)), MM_TO_INCH * 264.f))];
    iPad12_9Inch=iPad12_9Inch ? iPad12_9Inch : [AppleDeviceInfo deviceInfoWithResolution:CGSizeMake(2048, 2732) PPI:264.f EquavalentPoint:CGPointMakeWithGLKVector2(GLKVector2MultiplyScalar(GLKVector2Make(110.29f - (220.58f-196.61f)/2, - (19.95-11.08)), MM_TO_INCH * 264.f))];
    
    sizeProfileDict = sizeProfileDict ? sizeProfileDict : @{
                                                            @"MBPR15Inch":mbpr15,
                                                            @"MBPR13Inch":mbpr13,
                                                            @"MBA13Inch":mba13,
                                                            @"MBA11Inch":mba11,
                                                            @"MB12Inch":mb12,
                                                            @"iPhone4Inch":iPhone4Inch,
                                                            @"iPhone4_7Inch":iPhone4_7Inch,
                                                            @"iPhone5_5Inch":iPhone5_5Inch,
                                                            @"iPad7_9Inch":iPad7_9Inch,
                                                            @"iPad9_7Inch":iPad9_7Inch,
                                                            @"iPad12_9Inch":iPad12_9Inch};
    
    return sizeProfileDict[sizeProfileCode];
}

+ (NSString *) currentDeviceCode {
    NSString *code = nil;
#if TARGET_OS_IPHONE
    struct utsname systemInfo;
    uname(&systemInfo);
    code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
#else
    size_t len = 0;
    sysctlbyname("hw.model", NULL, &len, NULL, 0);
    if (len)
    {
        char *model = malloc(len*sizeof(char));
        sysctlbyname("hw.model", model, &len, NULL, 0);
        code = @(model);
        free(model);
    }
#endif
    return code;
}

+ (instancetype) currentDeviceProfile {
    NSString * deviceCode = [AppleDeviceInfo currentDeviceCode];
    NSString * deviceSizeProfileCode = [AppleDeviceInfo deviceCodeToDeviceSizeProfile:deviceCode];
    AppleDeviceInfo * deviceInfo = [AppleDeviceInfo deviceInfoBySizeProfile:deviceSizeProfileCode];
    CameraDeviceInfo * cameraInfo = [CameraDeviceInfo cameraProfileByDeviceCode:deviceCode];
    deviceInfo.frontCamera = cameraInfo;
    return deviceInfo;
}

+ (instancetype) deviceInfoWithResolution:(CGSize)resolution PPI:(float)screenPPI EquavalentPoint:(CGPoint)point {
    AppleDeviceInfo * info = [[AppleDeviceInfo alloc] init];
    info.screenPhysicalResolution = resolution;
    info.screenPhysicalPPI = screenPPI;
    info.equavalentPixelPointForFrontCamera = point;
    
    return info;
}

@end
