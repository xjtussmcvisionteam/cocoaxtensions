//
//  CGExtensions.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/30.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import <SceneKit/SceneKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import "NSValue+CommonNamings.h"

#if defined __cplusplus
extern "C" {
#endif
    
CGSize CGSizeMakeWithGLKVector2(GLKVector2 v2);
    
CGSize CGSizeMakeWithSCNVector3(SCNVector3 scnv3);
    
CGPoint CGPointMakeWithGLKVector2(GLKVector2 v2);

CGPoint CGPointMakeWithSCNVector3(SCNVector3 scnv3);
    
CGPoint CGPointMultiplyCGSize(CGPoint point, CGSize size);
    
CGPoint CGPointWithinCGRect(CGPoint point, CGRect rect);
    
CGPoint CGPointMultiplyScalar(CGPoint point, CGFloat scalar);

CGSize CGSizeMultiplyScalar(CGSize size, CGFloat scalar);

CGRect CGRectMakeWithCGPointAndCGSize(CGPoint origin, CGSize size);

CGRect CGRectSmallestBoundingxBoxForCGPoints(NSArray<NSValue *> * pointsArray);
    
CGFloat CGSizeArea(CGSize size);
    
CGFloat CGRectArea(CGRect rect);
    
CGPoint CGPointAddCGPoint(CGPoint p1, CGPoint p2);
    
#if defined __cplusplus
};
#endif
