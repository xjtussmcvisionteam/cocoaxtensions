//
//  CGExtensions.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/30.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "CGExtensions.h"

CGSize CGSizeMakeWithGLKVector2(GLKVector2 v2) {
    return CGSizeMake(v2.x, v2.y);
}

CGSize CGSizeMakeWithSCNVector3(SCNVector3 scnv3) {
    return CGSizeMake(scnv3.x, scnv3.y);
}

CGPoint CGPointMakeWithGLKVector2(GLKVector2 v2) {
    return CGPointMake(v2.x, v2.y);
}

CGPoint CGPointMakeWithSCNVector3(SCNVector3 scnv3) {
    return CGPointMake(scnv3.x, scnv3.y);
}

CGPoint CGPointMultiplyScalar(CGPoint point, CGFloat scalar) {
    return CGPointMake(point.x * scalar, point.y * scalar);
}

CGSize CGSizeMultiplyScalar(CGSize size, CGFloat scalar) {
    return CGSizeMake(size.width * scalar, size.height * scalar);
}

CGPoint CGPointWithinCGRect(CGPoint point, CGRect rect) {
    return CGPointMake((point.x - CGRectGetMinX(rect))/rect.size.width, (point.y - CGRectGetMinY(rect))/rect.size.height);
}

CGPoint CGPointMultiplyCGSize(CGPoint point, CGSize size) {
    return CGPointMake(point.x * size.width, point.y * size.height);
}

CGRect CGRectMakeWithCGPointAndCGSize(CGPoint origin, CGSize size) {
    return CGRectMake(origin.x, origin.y, size.width, size.height);
}

CGRect CGRectSmallestBoundingxBoxForCGPoints(NSArray<NSValue *> * pointsArray) {
    CGFloat greatestXValue = (pointsArray[0]).pointValue.x;
    CGFloat greatestYValue = (pointsArray[0]).pointValue.y;
    CGFloat smallestXValue = (pointsArray[0]).pointValue.x;
    CGFloat smallestYValue = (pointsArray[0]).pointValue.y;
    
    for(int i = 1; i < pointsArray.count; i++)
    {
        CGPoint point = (pointsArray[i]).pointValue;
        greatestXValue = MAX(greatestXValue, point.x);
        greatestYValue = MAX(greatestYValue, point.y);
        smallestXValue = MIN(smallestXValue, point.x);
        smallestYValue = MIN(smallestYValue, point.y);
    }
    
    CGRect rect;
    rect.origin = CGPointMake(smallestXValue, smallestYValue);
    rect.size.width = greatestXValue - smallestXValue;
    rect.size.height = greatestYValue - smallestYValue;
    
    return rect;
}

CGFloat CGSizeArea(CGSize size) {
    return size.width * size.height;
}

CGFloat CGRectArea(CGRect rect) {
    return rect.size.width * rect.size.height;
}

CGPoint CGPointAddCGPoint(CGPoint p1, CGPoint p2) {
    return CGPointMake(p1.x + p2.x, p1.y + p2.y);
}
