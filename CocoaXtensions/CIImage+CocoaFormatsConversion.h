//
//  CIImage+CocoaFormatsConversion.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/29.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <CoreImage/CoreImage.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import "NSUIImage.h"
#import "FoundationConversions.h"

@interface CIImage (CocoaFormatsConversion)

+ (nullable instancetype) imageWithContent:(nonnull id)content;

/**
 Universal CGImage producer. If the CIImage is CGImage backed, then retun the CGImage. Otherwise, create new CGImage and return.
 */
@property (nonatomic, nullable, readonly) CGImageRef cgImage;

@property (nonatomic, nullable, readonly) NSUIImage * nsuiImage;

@property (nonatomic, nullable, readonly) CVPixelBufferRef cvPixelBuffer;

@property (nonatomic, nullable, readonly) id<MTLTexture> mtlTexture;

- (nullable id<MTLTexture>) mtlTextureWithCommandBuffer:(nullable id<MTLCommandBuffer>)buffer;

- (nullable id<MTLTexture>) renderToMTLTexture:(nullable id<MTLTexture>) targetTexture commandBuffer:(nullable id<MTLCommandBuffer>)buffer;

#pragma mark - basic operations

- (void) saveToFile:(nonnull NSString *) filePathWithExtension;


# pragma mark - conversion shortcuts

+ (nullable CVPixelBufferRef) conversionCVPixelBufferFromCGImage:(nullable CGImageRef)cgImage;
+ (nullable CVPixelBufferRef) conversionCVPixelBufferFromNSUIImage:(nullable NSUIImage *)nsuiImage;
+ (nullable CVPixelBufferRef) conversionCVPixelBufferFromMTLTexture:(nullable id<MTLTexture>)mtlTexture;

+ (nullable CGImageRef) conversionCGImageFromCVPixelBuffer:(nullable CVPixelBufferRef)pixelBuffer;
+ (nullable CGImageRef) conversionCGImageFromFromNSUIImage:(nullable NSUIImage *)nsuiImage;
+ (nullable CGImageRef) conversionCGImageFromFromMTLTexture:(nullable id<MTLTexture>)mtlTexture;

+ (nullable NSUIImage *) conversionNSUIImageFromCVPixelBuffer:(nullable CVPixelBufferRef)pixelBuffer;
+ (nullable NSUIImage *) conversionNSUIImageFromCGImage:(nullable CGImageRef)cgImage;
+ (nullable NSUIImage *) conversionNSUIImageFromMTLTexture:(nullable id<MTLTexture>)mtlTexture;

+ (nullable id<MTLTexture>) conversionMTLTextureFromCGImage:(nullable CGImageRef)cgImage;
+ (nullable id<MTLTexture>) conversionMTLTextureFromCVPixelBuffer:(nullable CVPixelBufferRef)pixelBuffer;
+ (nullable id<MTLTexture>) conversionMTLTextureFromNSUIImage:(nullable NSUIImage *)nsuiImage;


@end
