//
//  CIImage+ExtendedCategories.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/19.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "CIImage+CocoaFormatsConversion.h"
#import "HeaderBase.h"

@interface CIImage (ExtendedCategories)

@property (nonatomic) float aspectRatio;

- (nonnull CIImage *) resizeTo:(CGSize)size;

- (nonnull CIImage *) resizeScaleKeepingAspectRatio:(CGFloat) ratio;

- (nonnull CIImage *) resizeToFillSizeKeepingAspectRatio:(CGSize)size;

- (nonnull CIImage *) rotateImage:(CGFloat)radian;

- (nonnull CIImage *) flipHorizontal;

- (nonnull CIImage *) flipVertical;

- (nonnull CIImage *) perspectiveCorrection:(nonnull NSArray<CIVector *> *) CIVectorArrayTlTrBlBr;

- (nonnull CIImage *) blend:(nonnull CIImage *) anotherImage WithFilter:(nonnull NSString *)blendFilterName;


@end
