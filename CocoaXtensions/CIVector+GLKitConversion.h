//
//  CIVector+GLKitConversion.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/30.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <CoreImage/CoreImage.h>
#import <SceneKit/SceneKit.h>
#import "GLKitExtensions.h"

@interface CIVector (GLKitConversion)

@property (nonatomic, readonly) SCNVector3 SCNVector3Value;

@property (nonatomic, readonly) SCNVector4 SCNVector4Value;

@property (nonatomic, readonly) GLKVector2 GLKVector2Value;

@property (nonatomic, readonly) GLKVector3 GLKVector3Value;

@property (nonatomic, readonly) GLKVector4 GLKVector4Value;

+ (instancetype) vectorWithGLKVector2:(GLKVector2) glkv2;

+ (instancetype) vectorWithGLKVector3:(GLKVector3) glkv3;

+ (instancetype) vectorWithGLKVector4:(GLKVector4) glkv4;

+ (instancetype) vectorWithSCNVector3:(SCNVector3) scnv3;

+ (instancetype) vectorWithSCNVector4:(SCNVector4) scnv4;

@end
