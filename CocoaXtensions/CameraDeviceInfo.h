//
//  CameraDeviceInfo.h
//  CocoaXtensions
//
//  Created by JiangZhping on 2016/11/16.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HeaderBase.h"

@interface CameraDeviceInfo : NSObject

@property (nonatomic, readonly) CGFloat focalLengthRatio;
@property (nonatomic, readonly) CGSize maxResolutionLandscape;
@property (nonatomic, readonly) CGPoint focalCenterPixel;
@property (nonatomic, readonly) CGPoint focalCenterDeviation;

// camera properties
@property (nonatomic, readonly) GLKMatrix3 cameraMatrix;
@property (nonatomic, readonly) CGFloat xFov;
@property (nonatomic, readonly) CGFloat yFov;
@property (nonatomic, readonly) CGFloat xSymmetricFov;
@property (nonatomic, readonly) CGFloat ySymmetricFov;
@property (nonatomic, readonly) CGSize symmetricFrameSize;
@property (nonatomic, readonly) CGRect croppingExtentInSymmetricFrame;

@property (nonatomic) CGSize currentFrameSize;
@property (nonatomic) UIDeviceOrientation deviceOrientation;

+ (instancetype) cameraProfileByDeviceCode:(NSString *)deviceCode;

- (GLKMatrix3) generateCameraMatrixWithFrameSize:(CGSize)frameSize;

@end
