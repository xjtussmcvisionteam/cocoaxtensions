//
//  TidiousConversions.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/15.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>
#import "GLKitExtensions.h"

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
NSString * NSStringFromPoint(CGPoint point);
NSString * NSStringFromSize(CGSize size);
NSString * NSStringFromRect(CGRect rect);
#else
#import <AppKit/AppKit.h>
NSString * NSStringFromCGPoint(CGPoint point);
NSString * NSStringFromCGSize(CGSize size);
NSString * NSStringFromCGRect(CGRect rect);
#endif

NSString * NSStringFromSCNVector3(SCNVector3 scnv3);
NSString * NSStringFromSCNVector4(SCNVector4 scnv4);
NSString * NSStringFromSCNMatrix4(SCNMatrix4 scnm4);
NSString * NSStringFromSCNQuaternion(SCNQuaternion scnq);
