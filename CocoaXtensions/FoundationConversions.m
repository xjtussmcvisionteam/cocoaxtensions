//
//  TidiousConversions.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/15.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "FoundationConversions.h"

#if TARGET_OS_IPHONE
NSString * NSStringFromPoint(CGPoint point) {
    return NSStringFromCGPoint(point);
}

NSString * NSStringFromSize(CGSize size) {
    return NSStringFromCGSize(size);
}

NSString * NSStringFromRect(CGRect rect) {
    return NSStringFromCGRect(rect);
}
#else

NSString * NSStringFromCGPoint(CGPoint point) {
    return NSStringFromPoint(point);
}

NSString * NSStringFromCGSize(CGSize size) {
    return NSStringFromSize(size);
}

NSString * NSStringFromCGRect(CGRect rect) {
    return NSStringFromRect(rect);
}

#endif

NSString * NSStringFromSCNVector3(SCNVector3 scnv3) {
    return NSStringFromGLKVector3(SCNVector3ToGLKVector3(scnv3));
}

NSString * NSStringFromSCNVector4(SCNVector4 scnv4) {
    return NSStringFromGLKVector4(SCNVector4ToGLKVector4(scnv4));
}

NSString * NSStringFromSCNMatrix4(SCNMatrix4 scnm4) {
    return NSStringFromGLKMatrix4(SCNMatrix4ToGLKMatrix4(scnm4));
}

NSString * NSStringFromSCNQuaternion(SCNQuaternion scnq) {
    return NSStringFromGLKQuaternion(GLKQuaternionMake(scnq.x, scnq.y, scnq.z, scnq.w));
}