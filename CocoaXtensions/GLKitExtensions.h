//
//  GLKConvenience.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/10.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

#if defined __cplusplus
extern "C" {
#endif
    
FOUNDATION_EXPORT const GLKVector2 GLKVector2Zero;
FOUNDATION_EXPORT const GLKVector3 GLKVector3Zero;
FOUNDATION_EXPORT const GLKVector4 GLKVector4Zero;

GLKVector2 GLKVector2FlipHorizontal(GLKVector2 v2);
    
GLKVector2 GLKVector2FlipVertical(GLKVector2 v2);

GLKVector2 GLKVector2MakeWithGLKVector3XY(GLKVector3 v3);
    
GLKVector2 GLKVector2MakeWithGLKVector3NormalizedByZ(GLKVector3 v3);

GLKVector2 GLKVector2MakeWithCGPoint(CGPoint point);
    
GLKVector2 GLKVector2MakeWithCGSize(CGSize size);

GLKVector3 GLKVector3MakeWithVector2(GLKVector2 vector, float z);
    
GLKVector2 GLKVector3GetGLKVector2(GLKVector3 vector);
    
GLKVector2 GLKMatrix3MultiplyVector2WithTranslation(GLKMatrix3 matrixLeft, GLKVector2 vectorRight);

GLKMatrix2 GLKMatrix2Make(float m00, float m01, float m10, float m11);

GLKMatrix4 GLKMatrix4MakeWithGLKMatrix3AndGLKVector3(GLKMatrix3 r, GLKVector3 t);

GLKMatrix4 GLKMatrix4MakeWithGLKMatrix3AtTLConer(GLKMatrix3 r);
    
#pragma mark - geometry
    
GLKVector3 GLKVector3InSphericalCoordinates(GLKVector3 v3);
    
GLKVector3 GLKPointProjectionOntoPlane(GLKVector3 point, GLKVector3 planeOrigin, GLKVector3 planeNormal);

#if defined __cplusplus
};
#endif
