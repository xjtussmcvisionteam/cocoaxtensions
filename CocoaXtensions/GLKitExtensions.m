//
//  GLKConvenience.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/10.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "GLKitExtensions.h"

const GLKVector2 GLKVector2Zero = {0,0};
const GLKVector3 GLKVector3Zero = {0,0,0};
const GLKVector4 GLKVector4Zero = {0,0,0,0};

GLKVector2 GLKVector2FlipHorizontal(GLKVector2 vector) {
    return GLKVector2Make(-vector.v[0], vector.v[1]);
}

GLKVector2 GLKVector2FlipVertical(GLKVector2 vector) {
    return GLKVector2Make(vector.v[0], -vector.v[1]);
}

GLKVector2 GLKVector2MakeWithGLKVector3XY(GLKVector3 vector) {
    return GLKVector2Make(vector.v[0], vector.v[1]);
}

GLKVector2 GLKVector2MakeWithGLKVector3NormalizedByZ(GLKVector3 vector) {
    return GLKVector2Make(vector.v[0]/vector.v[2], vector.v[1]/vector.v[2]);
}

GLKVector2 GLKVector2MakeWithCGPoint(CGPoint point) {
    return GLKVector2Make(point.x, point.y);
}

GLKVector2 GLKVector2MakeWithCGSize(CGSize size) {
    return GLKVector2Make(size.width, size.height);
}

GLKVector2 GLKVector3GetGLKVector2(GLKVector3 vector) {
    return GLKVector2Make(vector.v[0], vector.v[1]);
}

GLKVector3 GLKVector3MakeWithVector2(GLKVector2 vector, float z) {
    return GLKVector3Make(vector.v[0], vector.v[1], z);
}

GLKVector2 GLKMatrix3MultiplyVector2WithTranslation(GLKMatrix3 matrixLeft, GLKVector2 vectorRight) {
    GLKVector3 v3 = GLKMatrix3MultiplyVector3(matrixLeft,GLKVector3Make(vectorRight.v[0], vectorRight.v[1], 1.f));
    return GLKVector2Make(v3.v[0], v3.v[1]);
}

GLKMatrix2 GLKMatrix2Make(float m00, float m01, float m10, float m11) {
    return GLKMatrix3GetMatrix2(GLKMatrix3Make(m00,m01,0,m10,m11,0,0,0,0));
}

GLKMatrix4 GLKMatrix4MakeWithGLKMatrix3AndGLKVector3(GLKMatrix3 r, GLKVector3 t) {
    return GLKMatrix4Make(r.m00, r.m01, r.m02, 0, r.m10, r.m11, r.m12, 0, r.m20, r.m21, r.m22, 0, t.v[0], t.v[1], t.v[2], 1);
}

GLKMatrix4 GLKMatrix4MakeWithGLKMatrix3AtTLConer(GLKMatrix3 r) {
    return GLKMatrix4Make(r.m00, r.m01, r.m02, 0, r.m10, r.m11, r.m12, 0, r.m20, r.m21, r.m22, 0, 0,0,0,1.f);
}

GLKVector3 GLKVector3InSphericalCoordinates(GLKVector3 v3) {
    float azimuth = atan2(v3.x, v3.z);
    float elevation = atan2(v3.y,sqrt(pow(v3.x,2) + pow(v3.z,2)));
    float r = GLKVector3Length(v3);
    
    return GLKVector3Make(azimuth, elevation, r);
}

GLKVector3 GLKPointProjectionOntoPlane(GLKVector3 point, GLKVector3 planeOrigin, GLKVector3 planeNormal) {
    GLKVector3 normal = GLKVector3Normalize(planeNormal);
    GLKVector3 pointWRTOrigin = GLKVector3Subtract(point, planeOrigin);
    float height = GLKVector3DotProduct(pointWRTOrigin, normal);
    return GLKVector3Subtract(pointWRTOrigin, GLKVector3MultiplyScalar(normal, height));
}
