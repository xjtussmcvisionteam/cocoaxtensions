//
//  HeaderBase.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/19.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#ifndef __COCOA_XTENSIONS_HEADERBASE__
#define __COCOA_XTENSIONS_HEADERBASE__



#import <Foundation/Foundation.h>

#ifndef ScreenCameraInfo_hpp

#define INCH_TO_MM 25.4f
#define MM_TO_INCH (1.0/25.4)

#if !TARGET_OS_IPHONE
// this is a copy of the same definition in iOS code
typedef NS_ENUM(NSInteger, UIDeviceOrientation) {
    UIDeviceOrientationUnknown,
    UIDeviceOrientationPortrait,            // Device oriented vertically, home button on the bottom
    UIDeviceOrientationPortraitUpsideDown,  // Device oriented vertically, home button on the top
    UIDeviceOrientationLandscapeLeft,       // Device oriented horizontally, home button on the right
    UIDeviceOrientationLandscapeRight,      // Device oriented horizontally, home button on the left
    UIDeviceOrientationFaceUp,              // Device oriented flat, face up
    UIDeviceOrientationFaceDown             // Device oriented flat, face down
};

#endif

#endif

#import "GLKitExtensions.h"
#import "NSValue+CommonNamings.h"
#import "CGExtensions.h"
#import "NSNumber+Random.h"
#import "NSTimer+TimerWithBlock.h"

#endif
