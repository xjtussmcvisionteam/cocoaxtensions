//
//  NSNumber+Random.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/9.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "NSNumber+Random.h"

@implementation NSNumber (Random)

+ (CGFloat)randomFloat {
    return [[GKRandomSource sharedRandom] nextUniform];
}

+ (CGFloat)randomFloatSmallerThan:(CGFloat)maxValue {
    return maxValue * [NSNumber randomFloat];
}

+ (CGFloat)randomFloatBetween:(CGFloat)minValue and:(CGFloat)maxValue {
    CGFloat realMin = MIN(minValue, maxValue);
    CGFloat realMax = MAX(minValue,maxValue);
    return realMin + (realMax - realMin) * [NSNumber randomFloat];
}

+ (NSInteger)randomIntegerBetween:(NSInteger)minValue and:(NSInteger)maxValue {
    NSInteger realMin = MIN(minValue, maxValue);
    NSInteger realMax = MAX(minValue,maxValue);
    
    return realMin + (realMax - realMin) * [NSNumber randomFloat];
}

+ (NSInteger)randomIntegerSmallerThan:(NSInteger)maxValue {
    return [NSNumber randomIntegerBetween:0 and:maxValue];
}

+ (NSInteger)randomInteger {
    return [NSNumber randomIntegerBetween:0 and:100];
}

+ (BOOL)randomBOOL {
    return [[GKRandomSource sharedRandom] nextBool];
}

@end
