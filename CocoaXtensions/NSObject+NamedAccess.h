//
//  NSObject+AccessByName.h
//  CocoaXtensions
//
//  Created by JiangZhping on 2016/10/1.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NamedAccess)

@property (nonatomic) NSString * codeInNamedAccess;

+ (NSDictionary<NSString *,id> *)namedObjectDictionary;

+ (id) retrieveObjectWithName:(NSString *)name;

+ (void) letObject:(id)object accessibleByName:(NSString *)name;


# pragma mark - advanced notification/ blocking

+ (id) retrieveObjectWithName:(NSString *)name blockingUntil:(NSDate *)dateToStopBlocking;

+ (void) retrieveObjectWithName:(NSString *)name AsyncWithBlock:(void (^) (id retrievedObject))handleBlock;

@end
