//
//  NSOperationQueue+SharedQueue.h
//  AutoStereogramMetal
//
//  Created by 蒋志平 on 16/7/7.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSOperationQueue (SharedQueue)

+ (NSOperationQueue * ) sharedSerialOperationQueue;
+ (NSOperationQueue * ) sharedConcurrentOperationQueue;
+ (NSOperationQueue *) concurrentQueueNamed:(NSString *)queueName;
+ (NSOperationQueue *) serialQueueNamed:(NSString *)queueName;

@end
