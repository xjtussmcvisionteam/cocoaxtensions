//
//  NSOperationQueue+SharedQueue.m
//  AutoStereogramMetal
//
//  Created by 蒋志平 on 16/7/7.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "NSOperationQueue+SharedQueue.h"

@implementation NSOperationQueue (SharedQueue)

+ (NSOperationQueue * ) sharedSerialOperationQueue {
    static NSOperationQueue* sharedQueue = nil;
    if (sharedQueue == nil) {
        dispatch_queue_t queue = dispatch_queue_create("operations",DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue( queue, dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0 ) );
        sharedQueue = [NSOperationQueue mainQueue];
        sharedQueue.underlyingQueue = queue;
        sharedQueue.maxConcurrentOperationCount = 1;
    }
    return sharedQueue;
}

+ (NSOperationQueue * ) sharedConcurrentOperationQueue {
    static NSOperationQueue* sharedQueue = nil;
    if (sharedQueue == nil) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        sharedQueue = [[NSOperationQueue alloc] init];
        sharedQueue.underlyingQueue = queue;
        sharedQueue.maxConcurrentOperationCount = 10;
    }
    return sharedQueue;
}

+ (NSOperationQueue *) concurrentQueueNamed:(NSString *)queueName {
    dispatch_queue_t queue = dispatch_queue_create(queueName.UTF8String, DISPATCH_QUEUE_CONCURRENT);
    dispatch_set_target_queue( queue, dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0 ) );
    NSOperationQueue* sharedQueue  = [[NSOperationQueue alloc] init];
    sharedQueue.underlyingQueue = queue;
    return sharedQueue;
}

+ (NSOperationQueue *) serialQueueNamed:(NSString *)queueName {
    dispatch_queue_t queue = dispatch_queue_create(queueName.UTF8String, DISPATCH_QUEUE_SERIAL);
    dispatch_set_target_queue( queue, dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0 ) );
    NSOperationQueue* sharedQueue  = [[NSOperationQueue alloc] init];
    sharedQueue.underlyingQueue = queue;
    return sharedQueue;
}

@end
