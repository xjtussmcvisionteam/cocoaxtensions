//
//  NSString+ExtendedUserDefaults.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/20.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSUserDefaults+CheckExistsBeforeSetting.h"
#import "NSObject+NamedAccess.h"

@interface NSString (ExtendedUserDefaults)


#pragma mark - NSUserDefaults related
- (BOOL) keyExistsInUserDefaults;

- (BOOL) boolValueInUserDefaults;
- (double) doubleValueInUserDefaults;
-(float) floatValueInUserDefaults;
-(NSInteger) integerValueInUserDefaults;
-(nullable id) objectValueInUserDefaults;
-(nullable NSString *) stringValueInUserDefaults;
-(nullable NSURL *) urlValueInUserDefaults;

- (BOOL) boolValueInUserDefaultsWithValueForNonExistentKey:(BOOL)defaultValue;
- (double) doubleValueInUserDefaultsWithValueForNonExistentKey:(double)defaultValue;
-(float) floatValueInUserDefaultsWithValueForNonExistentKey:(float)defaultValue;
-(NSInteger) integerValueInUserDefaultsWithValueForNonExistentKey:(NSInteger)defaultValue;
-(nullable id) objectValueInUserDefaultsWithValueForNonExistentKey:(nonnull id)defaultValue;
-(nullable NSString *) stringValueInUserDefaultsWithValueForNonExistentKey:(nonnull NSString *)defaultValue;
-(nullable NSURL *) urlValueInUserDefaultsWithValueForNonExistentKey:(nonnull NSURL *)defaultValue;

- (void) setBoolInUseDefaults:(BOOL)defaultValue;
- (void) setFloatInUseDefaults:(float)defaultValue;
- (void) setDoubleInUseDefaults:(double)defaultValue;
- (void) setIntegerInUseDefaults:(NSInteger)defaultValue;
- (void) setObjectInUseDefaults:(nonnull id)defaultValue;
- (void) setURLInUseDefaults:(nonnull NSURL *)defaultValue;

- (void) setBoolInUseDefaultsIfKeyNotExistent:(BOOL)defaultValue;
- (void) setFloatInUseDefaultsIfKeyNotExistent:(float)defaultValue;
- (void) setDoubleInUseDefaultsIfKeyNotExistent:(double)defaultValue;
- (void) setIntegerInUseDefaultsIfKeyNotExistent:(NSInteger)defaultValue;
- (void) setObjectInUseDefaultsIfKeyNotExistent:(nonnull id)defaultValue;
- (void) setURLInUseDefaultsIfKeyNotExistent:(nonnull NSURL *)defaultValue;

#pragma mark - Named Access

- (nullable id) associatedObjectUnderNamedAccess;

@end
