//
//  NSString+ExtendedUserDefaults.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/20.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "NSString+ExtendedUserDefaults.h"

@implementation NSString (ExtendedUserDefaults)

- (BOOL) keyExistsInUserDefaults {
    return [[NSUserDefaults standardUserDefaults] keyExists:self];
}

- (BOOL) boolValueInUserDefaults {
    return [[NSUserDefaults standardUserDefaults] boolForKey:self];
}

- (double) doubleValueInUserDefaults {
	return [[NSUserDefaults standardUserDefaults] doubleForKey:self];
}

-(float) floatValueInUserDefaults {
	return [[NSUserDefaults standardUserDefaults] floatForKey:self];
}

-(NSInteger) integerValueInUserDefaults {
	return [[NSUserDefaults standardUserDefaults] integerForKey:self];
}

-(NSString *) stringValueInUserDefaults {
    return [[NSUserDefaults standardUserDefaults] stringForKey:self];
}

-(NSURL *) urlValueInUserDefaults {
    return [[NSUserDefaults standardUserDefaults] URLForKey:self];
}

- (id) objectValueInUserDefaults {
    return [[NSUserDefaults standardUserDefaults] objectForKey:self];
}

- (BOOL) boolValueInUserDefaultsWithValueForNonExistentKey:(BOOL)defaultValue {
	return [[NSUserDefaults standardUserDefaults] boolForKey:self ifKeyNotExistsSetDefault:defaultValue];
}

- (double) doubleValueInUserDefaultsWithValueForNonExistentKey:(double)defaultValue {
	return [[NSUserDefaults standardUserDefaults] doubleForKey:self ifKeyNotExistsSetDefault:defaultValue];
}

-(float) floatValueInUserDefaultsWithValueForNonExistentKey:(float)defaultValue {
	return [[NSUserDefaults standardUserDefaults] floatForKey:self ifKeyNotExistsSetDefault:defaultValue];
}

-(NSInteger) integerValueInUserDefaultsWithValueForNonExistentKey:(NSInteger)defaultValue {
	return [[NSUserDefaults standardUserDefaults] integerForKey:self ifKeyNotExistsSetDefault:defaultValue];
}

-(NSString *) stringValueInUserDefaultsWithValueForNonExistentKey:(NSString *)defaultValue {
    return [[NSUserDefaults standardUserDefaults] stringForKey:self ifKeyNotExistsSetDefault:defaultValue];
}

-(NSURL *)urlValueInUserDefaultsWithValueForNonExistentKey:(NSURL *)defaultValue {
    return [[NSUserDefaults standardUserDefaults] URLForKey:self ifKeyNotExistsSetDefault:defaultValue];
}

- (id)objectValueInUserDefaultsWithValueForNonExistentKey:(id)defaultValue {
    return [[NSUserDefaults standardUserDefaults] objectForKey:self ifKeyNotExistsSetDefault:defaultValue];
}

- (void) setBoolInUseDefaults:(BOOL)defaultValue {
    [[NSUserDefaults standardUserDefaults] setBool:defaultValue forKey:self];
}

- (void) setFloatInUseDefaults:(float)defaultValue {
	[[NSUserDefaults standardUserDefaults] setFloat:defaultValue forKey:self];
}

- (void) setDoubleInUseDefaults:(double)defaultValue {
	[[NSUserDefaults standardUserDefaults] setDouble:defaultValue forKey:self];
}

- (void) setIntegerInUseDefaults:(NSInteger)defaultValue {
	[[NSUserDefaults standardUserDefaults] setInteger:defaultValue forKey:self];
}

- (void) setObjectInUseDefaults:(nonnull id)defaultValue {
	[[NSUserDefaults standardUserDefaults] setObject:defaultValue forKey:self];
}

- (void) setURLInUseDefaults:(nonnull NSURL *)defaultValue {
	[[NSUserDefaults standardUserDefaults] setURL:defaultValue forKey:self];
}

- (void) setBoolInUseDefaultsIfKeyNotExistent:(BOOL)defaultValue {
	[[NSUserDefaults standardUserDefaults] setBool:defaultValue ifKeyNotExists:self];
}

- (void) setFloatInUseDefaultsIfKeyNotExistent:(float)defaultValue {
	[[NSUserDefaults standardUserDefaults] setFloat:defaultValue ifKeyNotExists:self];
}

- (void) setDoubleInUseDefaultsIfKeyNotExistent:(double)defaultValue {
	[[NSUserDefaults standardUserDefaults] setDouble:defaultValue ifKeyNotExists:self];
}

- (void) setIntegerInUseDefaultsIfKeyNotExistent:(NSInteger)defaultValue {
	[[NSUserDefaults standardUserDefaults] setInteger:defaultValue ifKeyNotExists:self];
}

- (void) setObjectInUseDefaultsIfKeyNotExistent:(nonnull id)defaultValue {
	[[NSUserDefaults standardUserDefaults] setObject:defaultValue ifKeyNotExists:self];
}

- (void) setURLInUseDefaultsIfKeyNotExistent:(nonnull NSURL *)defaultValue {
	[[NSUserDefaults standardUserDefaults] setURL:defaultValue ifKeyNotExists:self];
}

- (id) associatedObjectUnderNamedAccess {
    return [NSObject retrieveObjectWithName:self];
}
@end
