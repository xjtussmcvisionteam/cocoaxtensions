//
//  NSTimer+TimerWithBlock.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/16.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (TimerWithBlock)

+(NSTimer *) scheduledTimerWithTimeInterval:(NSTimeInterval)inTimeInterval block:(void (^)(void))inBlock repeats:(BOOL)yesOrNo;
+(NSTimer *) timerWithTimeInterval:(NSTimeInterval)inTimeInterval block:(void (^)(void))inBlock repeats:(BOOL)inRepeats;

@end
