//
//  NSUIColor+ColorComponents.h
//  CocoaXtensions
//
//  Created by JiangZhping on 2016/10/27.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <CocoaXtensions/CocoaXtensions.h>

#import <Foundation/Foundation.h>
#if !TARGET_OS_IPHONE
#import <AppKit/AppKit.h>
@interface NSColor (ColorComponents)
#else
#import <UIKit/UIKit.h>
@interface UIColor (ColorComponents)

@property (nonatomic, readonly) CGFloat redComponent;
@property (nonatomic, readonly) CGFloat greenComponent;
@property (nonatomic, readonly) CGFloat blueComponent;
@property (nonatomic, readonly) CGFloat alphaComponent;
#endif

@end




