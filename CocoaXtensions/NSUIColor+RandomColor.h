//
//  NSColor+RandomColor.h
//  VADSDelegates
//
//  Created by JiangZhiping on 16/3/31.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "NSUIColor.h"
#import "NSNumber+Random.h"

@interface NSUIColor (RandomColor)

+ (NSUIColor *) randomColor;

@end
