//
//  NSColor+RandomColor.m
//  VADSDelegates
//
//  Created by JiangZhiping on 16/3/31.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "NSUIColor+RandomColor.h"

@implementation NSUIColor (RandomColor)


+ (NSUIColor *) randomColor  {
    CGFloat red =  [NSNumber randomFloat];
    CGFloat blue = [NSNumber randomFloat];
    CGFloat green = [NSNumber randomFloat];
    return (NSUIColor *)[NSUIColor colorWithRed:red green:green blue:blue alpha:1.0];
}

@end
