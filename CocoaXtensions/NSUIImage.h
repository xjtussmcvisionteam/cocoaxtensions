//
//  NSUIImage.h
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/6.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#if TARGET_OS_IPHONE
    #import <UIKit/UIKit.h>
    @interface NSUIImage : UIImage
#else
    #import <AppKit/AppKit.h>
    @interface NSUIImage : NSImage
#endif

@end
