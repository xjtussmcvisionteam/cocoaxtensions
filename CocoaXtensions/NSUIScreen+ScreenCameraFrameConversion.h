//
//  NSUIScreen+ScreenCameraFrameConversion.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/14.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <sys/utsname.h>
#include <sys/sysctl.h>
#import "NSUIScreen.h"
#import "GLKitExtensions.h"
#import "AppleDeviceInfo.h"

#if TARGET_OS_IPHONE
@interface UIScreen (ScreenCameraFrameConversion)
#else
@interface NSScreen (ScreenCameraFrameConversion)
#endif

#if TARGET_OS_IPHONE
@property (nonatomic, readonly) CGFloat backingScaleFactor;
#endif

@property (nonatomic, readonly) CGSize screenPhysicalResolution;

@property (nonatomic, readonly) float screenPhysicalPPI;

@property (nonatomic, readonly) CGPoint frontCameraPixelInPhysicalResolution;

@property (nonatomic, readonly) GLKMatrix3 transformationFromScreenToCamera;

@property (nonatomic, readonly) CGSize currentDisplayResolution;

@property (nonatomic, readonly) float currentDisplayRatio;

@property (nonatomic, readonly) float currentDisplayPPI;

@property (nonatomic, readonly) CGPoint frontCameraPixelInCurrentResolution;

@property (nonatomic, readonly) GLKMatrix4 transformFromFrontCameraFrameToSceneModelCenteredOnScreenCenter;

- (double) screenRotationForOrientation:(UIDeviceOrientation)orientation;

- (GLKVector2) transformPixel:(GLKVector2)pointA fromOrientationA:(UIDeviceOrientation)deviceOrientationA intoB:(UIDeviceOrientation)deviceOrientationB;

- (GLKVector3) frontCameraPointFromPixel:(GLKVector2)pixel WithDeviceOrientation:(UIDeviceOrientation)deviceOrientation;

- (GLKVector3) frontCameraPointFromPixel:(GLKVector2)pixel;

- (GLKVector2) pixelFromFrontCameraPoint:(GLKVector3) point WithDeviceOrientation:(UIDeviceOrientation)deviceOrientation;

- (GLKVector2) pixelFromFrontCameraPoint:(GLKVector3) point;

- (GLKVector3) sceneModelScreenCenterFramePointFromFrontCamearPoint:(GLKVector3) point;

@end
