//
//  NSUIScreen.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/14.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>

#if !TARGET_OS_IPHONE
#import <AppKit/AppKit.h>
@interface NSUIScreen : NSScreen

#else
#import <UIKit/UIKit.h>
@interface NSUIScreen : UIScreen
#endif

@end