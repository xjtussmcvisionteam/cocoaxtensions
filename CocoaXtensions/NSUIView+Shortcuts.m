//
//  NSUIView+Shortcuts.m
//  CocoaXtensions
//
//  Created by JiangZhping on 2016/10/1.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "NSUIView+Shortcuts.h"

#import <Foundation/Foundation.h>
#if !TARGET_OS_IPHONE
#import <AppKit/AppKit.h>
@implementation NSView (Shortcuts)


#else
#import <UIKit/UIKit.h>
@implementation UIView (Shortcuts)
#endif

@dynamic viewAspectRatio;

- (float) viewAspectRatio {
    return self.bounds.size.width / self.bounds.size.height;
}

- (void) setViewAspectRatio:(float)viewAspectRatio {
    if (!self.superview || self.viewAspectRatio == viewAspectRatio) {
        return;
    }
    
    CGRect myBounds = self.superview.bounds;
    float parentRatio = myBounds.size.width / myBounds.size.height;
    
    if (parentRatio >= viewAspectRatio) {
        myBounds.size.width = myBounds.size.height * viewAspectRatio;
    } else {
        myBounds.size.height = myBounds.size.width / viewAspectRatio;
    }
    
#if !TARGET_OS_IPHONE
    [self setFrameOrigin:CGPointMake(self.superview.bounds.size.width/2.0, self.superview.bounds.size.height/2.0)];
    self.frame = myBounds;
#else
    self.center = CGPointMake(self.superview.bounds.size.width/2.0, self.superview.bounds.size.height/2.0);
    self.bounds = myBounds;
#endif

}

- (void)addConstraintFillParentWidth {
    NSUIView * parentView = (NSUIView *) self.superview;
    NSDictionary * layoutElements = @{@"self" : self };
    [parentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[self]-0-|" options:0 metrics:nil views:layoutElements]];
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
}

- (void)addConstraintFillParentHeight {
    NSUIView * parentView = (NSUIView *) self.superview;
    NSDictionary * layoutElements = @{@"self" : self };
    [parentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[self]-0-|" options:0 metrics:nil views:layoutElements]];
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
}


@end
