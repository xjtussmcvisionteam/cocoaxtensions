//
//  NSUIVIew.h
//  CameraPipeline
//
//  Created by JiangZhiping on 15/11/3.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#if !TARGET_OS_IPHONE
#import <AppKit/AppKit.h>
@interface NSUIView : NSView


#else
#import <UIKit/UIKit.h>
@interface NSUIView : UIView
#endif

@end
