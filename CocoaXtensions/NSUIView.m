//
//  NSUIVIew.m
//  CameraPipeline
//
//  Created by JiangZhiping on 15/11/3.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import "NSUIVIew.h"

@implementation NSUIView

- (instancetype) init {
    self = [super init];
    if (self) {
#if !TARGET_OS_IPHONE
        [self setWantsLayer:YES];
#endif
    }
    return self;
}


- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
#if !TARGET_OS_IPHONE
        [self setWantsLayer:YES];
#endif
    }
    return self;
}


- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
#if !TARGET_OS_IPHONE
        [self setWantsLayer:YES];
#endif
    }
    return self;
}

@end
