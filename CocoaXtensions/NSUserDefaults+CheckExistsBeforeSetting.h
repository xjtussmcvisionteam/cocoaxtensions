//
//  NSUserDefaults+CheckExistsBeforeSetting.h
//  AutostereogramApp
//
//  Created by JiangZhping on 16/8/12.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (CheckExistsBeforeSetting)

- (BOOL) keyExists:(nonnull NSString *) defaultName;

#pragma mark - setter and getter in case of non-existent key

- (void) setBool:(BOOL)value ifKeyNotExists:(nonnull NSString *)defaultName;
- (void) setFloat:(float)value ifKeyNotExists:(nonnull NSString *)defaultName;
- (void) setInteger:(NSInteger)value ifKeyNotExists:(nonnull NSString *)defaultName;
- (void) setObject:(nullable id)value ifKeyNotExists:(nonnull NSString *)defaultName;
- (void) setDouble:(float)value ifKeyNotExists:(nonnull NSString *)defaultName;
- (void) setURL:(nullable NSURL *)value ifKeyNotExists:(nonnull NSString *)defaultName;

- (BOOL) boolForKey:(nonnull NSString *)defaultName ifKeyNotExistsSetDefault:(BOOL) value;
- (double) doubleForKey:(nonnull NSString *)defaultName ifKeyNotExistsSetDefault:(double) value;
- (float) floatForKey:(nonnull NSString *)defaultName ifKeyNotExistsSetDefault:(float) value;
- (NSInteger) integerForKey:(nonnull NSString *)defaultName ifKeyNotExistsSetDefault:(NSInteger) value;
- (nullable id) objectForKey:(nonnull NSString *)defaultName ifKeyNotExistsSetDefault:(nullable id) value;
- (nullable NSString *) stringForKey:(nonnull NSString *)defaultName ifKeyNotExistsSetDefault:(nullable NSString *) value;
- (nullable NSURL *) URLForKey:(nonnull NSString *)defaultName ifKeyNotExistsSetDefault:(nullable NSURL *) value;


@end
