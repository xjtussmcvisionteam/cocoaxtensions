//
//  NSValue+CommonNamings.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/3.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "NSValue+CommonNamings.h"

@implementation NSValue (CommonNamings)

#if TARGET_OS_IPHONE

+ (instancetype) valueWithPoint:(CGPoint)point {
    return [NSValue valueWithCGPoint:point];
}

+ (instancetype) valueWithSize:(CGSize)size {
    return [NSValue valueWithCGSize:size];
}

+ (instancetype) valueWithRect:(CGRect)rect {
    return [NSValue valueWithCGRect:rect];
}

- (CGPoint) pointValue {
    return [self CGPointValue];
}

- (CGRect) rectValue {
    return [self CGRectValue];
}
- (CGSize) sizeValue {
    return [self CGSizeValue];
}

#else

+ (instancetype) valueWithCGPoint:(CGPoint)point {
    return [NSValue valueWithPoint:point];
}

+ (instancetype) valueWithCGSize:(CGSize)size {
    return [NSValue valueWithSize:size];
}

+ (instancetype) valueWithCGRect:(CGRect)rect {
    return [NSValue valueWithRect:rect];
}

- (CGPoint) CGPointValue {
    return self.pointValue;
}
- (CGRect) CGRectValue {
    return self.rectValue;
}
- (CGSize) CGSizeValue {
    return self.sizeValue;
}

#endif

@end
