//
//  SCNNode+Shortcuts.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/22.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <SceneKit/SceneKit.h>
#import "GLKitExtensions.h"
#import "CGExtensions.h"
#import "CIVector+GLKitConversion.h"

@interface SCNNode (Shortcuts)

@property (nonatomic, readonly) SCNNode * rootNode;
@property (nonatomic) SCNVector3 worldPosition;
@property (nonatomic) SCNMatrix4 worldTransform;

@property (nonatomic) GLKVector3 glkPosition;
@property (nonatomic) GLKVector3 glkWorldPosition;
@property (nonatomic) GLKVector4 glkRotation;
@property (nonatomic) GLKVector3 glkEulerAngles;
@property (nonatomic) GLKQuaternion glkOrientation;
@property (nonatomic) GLKVector3 glkScale;
@property (nonatomic) GLKMatrix4 glkPivot;
@property (nonatomic) GLKMatrix4 glkTransform;
@property (nonatomic) GLKMatrix4 glkWorldTransform;

@property (nonatomic) id diffuseContentOf1stMaterial;

- (GLKVector3) glkConvertPosition:(GLKVector3)position toNode:(SCNNode *)node;
- (GLKVector3) glkConvertPosition:(GLKVector3)position fromNode:(SCNNode *)node;
- (GLKMatrix4)glkConvertTransform:(GLKMatrix4)transform fromNode:(SCNNode *)node;
- (GLKMatrix4)glkConvertTransform:(GLKMatrix4)transform toNode:(SCNNode *)node;

- (NSArray<SCNHitTestResult *> *)glkHitTestWithSegmentFromPoint:(GLKVector3)pointA toPoint:(GLKVector3)pointB options:(NSDictionary<NSString *,id> *)options;

/*!
 @brief Advanced "==" operation, checking if belongs to the whole hierarchy
 */
- (BOOL) belongsToNode:(SCNNode *) tree;

- (SCNNode *) deepCopy;

- (CGPoint) cameraPixelProjectionForPosition:(SCNVector3)point fromNode:(SCNNode *) sourceNode imageSize:(CGSize)imageSize;


- (void) cameraGazeAt:(GLKVector3)aWorldPosition;

- (void) narrowXYFovToCover:(NSArray<CIVector *> *)points;

@end
