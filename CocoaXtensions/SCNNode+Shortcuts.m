//
//  SCNNode+Shortcuts.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/22.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "SCNNode+Shortcuts.h"

@implementation SCNNode (Shortcuts)

@dynamic rootNode;
@dynamic worldPosition;
@dynamic worldTransform;
@dynamic glkPosition;
@dynamic glkRotation;
@dynamic glkEulerAngles;
@dynamic glkWorldPosition;
@dynamic glkOrientation;
@dynamic glkScale;
@dynamic glkPivot;
@dynamic glkTransform;
@dynamic glkWorldTransform;

@dynamic diffuseContentOf1stMaterial;

- (SCNNode *) rootNode {
    if (self.parentNode.parentNode) {
        return (self.parentNode).rootNode;
    } else {
        return self.parentNode;
    }
}

- (SCNVector3) worldPosition {
    SCNNode * parentNode = self.parentNode;
    if (parentNode) {
        return [parentNode convertPosition:self.position toNode:nil];
    } else {
        return self.position;
    }
}

- (void)setWorldPosition:(SCNVector3)worldPosition {
    self.position = [self.parentNode convertPosition:worldPosition fromNode:nil];
}

-(void)setWorldTransform:(SCNMatrix4)worldTransform {
    self.transform = [self.parentNode convertTransform:worldTransform fromNode:nil];
}

#pragma mark - GLK getters

- (GLKMatrix4)glkTransform {
    return SCNMatrix4ToGLKMatrix4(self.transform);
}

- (GLKVector3)glkPosition {
    return SCNVector3ToGLKVector3(self.position);
}

- (GLKVector4)glkRotation {
    return SCNVector4ToGLKVector4(self.rotation);
}

- (GLKVector3)glkEulerAngles {
    return SCNVector3ToGLKVector3(self.eulerAngles);
}

-(GLKQuaternion)glkOrientation {
    return GLKQuaternionMake(self.orientation.x, self.orientation.y, self.orientation.z, self.orientation.w);
}

- (GLKVector3)glkScale {
    return SCNVector3ToGLKVector3(self.scale);
}

- (GLKMatrix4)glkPivot {
    return SCNMatrix4ToGLKMatrix4(self.pivot);
}

- (GLKMatrix4)glkWorldTransform {
    return SCNMatrix4ToGLKMatrix4(self.worldTransform);
}

-(GLKVector3)glkWorldPosition {
    return SCNVector3ToGLKVector3(self.worldPosition);
}

- (id) diffuseContentOf1stMaterial {
    return self.geometry.firstMaterial.diffuse.contents;
}

#pragma mark - GLK setters

- (void)setGlkTransform:(GLKMatrix4)glkTransform {
    self.transform = SCNMatrix4FromGLKMatrix4(glkTransform);
}

- (void)setGlkPosition:(GLKVector3)glkPosition {
    self.position = SCNVector3FromGLKVector3(glkPosition);
}

- (void)setGlkRotation:(GLKVector4)glkRotation {
    self.rotation = SCNVector4FromGLKVector4(self.glkRotation);
}

- (void)setGlkEulerAngles:(GLKVector3)glkEulerAngles {
    self.eulerAngles = SCNVector3FromGLKVector3(glkEulerAngles);
}

- (void)setGlkOrientation:(GLKQuaternion)glkOrientation {
    self.orientation = SCNVector4Make(glkOrientation.x, glkOrientation.y, glkOrientation.z, glkOrientation.w);
}

- (void)setGlkScale:(GLKVector3)glkScale {
    self.scale = SCNVector3FromGLKVector3(glkScale);
}

- (void)setGlkPivot:(GLKMatrix4)glkPivot {
    self.pivot = SCNMatrix4FromGLKMatrix4(glkPivot);
}

- (void)setGlkWorldTransform:(GLKMatrix4)glkWorldTransform {
    self.worldTransform = SCNMatrix4FromGLKMatrix4(glkWorldTransform);
}

- (void)setGlkWorldPosition:(GLKVector3)glkWorldPosition {
    self.worldPosition = SCNVector3FromGLKVector3(glkWorldPosition);
}

- (void)setDiffuseContentOf1stMaterial:(id)diffuseContentOf1stMaterial {
    self.geometry.firstMaterial.diffuse.contents = diffuseContentOf1stMaterial;
}

-(GLKVector3)glkConvertPosition:(GLKVector3)position toNode:(SCNNode *)node {
    return SCNVector3ToGLKVector3([self convertPosition:SCNVector3FromGLKVector3(position) toNode:node]);
}

- (GLKVector3)glkConvertPosition:(GLKVector3)position fromNode:(SCNNode *)node {
    return SCNVector3ToGLKVector3([self convertPosition:SCNVector3FromGLKVector3(position) fromNode:node]);
}

- (GLKMatrix4)glkConvertTransform:(GLKMatrix4)transform toNode:(SCNNode *)node {
    return SCNMatrix4ToGLKMatrix4([self convertTransform:SCNMatrix4FromGLKMatrix4(transform) toNode:node]);
}

- (GLKMatrix4)glkConvertTransform:(GLKMatrix4)transform fromNode:(SCNNode *)node {
    return SCNMatrix4ToGLKMatrix4([self convertTransform:SCNMatrix4FromGLKMatrix4(transform) fromNode:node]);
}

- (NSArray<SCNHitTestResult *> *)glkHitTestWithSegmentFromPoint:(GLKVector3)pointA toPoint:(GLKVector3)pointB options:(NSDictionary<NSString *,id> *)options {
    return [self hitTestWithSegmentFromPoint:SCNVector3FromGLKVector3(pointA) toPoint:SCNVector3FromGLKVector3(pointB) options:options];
}

- (BOOL) belongsToNode:(SCNNode *) tree {
    __block BOOL result = NO;
    if (self == tree) {
        return YES;
    }
	[tree enumerateChildNodesUsingBlock:^(SCNNode * _Nonnull child, BOOL * _Nonnull stop) {
        if (child == self) {
            result = YES;
            *stop = YES;
        }
    }];
    return result;
}

- (SCNNode *) deepCopy {
    SCNNode *clonedNode = [self clone];
    clonedNode.geometry = [self.geometry copy];
    clonedNode.geometry.firstMaterial = [self.geometry.firstMaterial copy];
    clonedNode.light = [self.light copy];
    clonedNode.camera = [ self.camera copy];
    
    return clonedNode;
}


- (void) cameraGazeAt:(GLKVector3)aWorldPosition {
    GLKVector3 upVector = GLKVector3Normalize([self.parentNode glkConvertPosition:GLKVector3Add(GLKVector3Make(0, 1000 , 0), self.glkWorldPosition) fromNode:nil]);
    GLKVector3 negativeGazeVector = GLKVector3Normalize(GLKVector3Subtract(self.glkPosition, [self.rootNode glkConvertPosition:aWorldPosition toNode:self.parentNode]));
    GLKVector3 rightVector = GLKVector3Negate(GLKVector3CrossProduct(negativeGazeVector, upVector));
    GLKMatrix3 rotation = GLKMatrix3MakeWithColumns(rightVector, upVector, negativeGazeVector);
    self.glkOrientation = GLKQuaternionMakeWithMatrix3(rotation);
}

- (void) narrowXYFovToCover:(NSArray<CIVector *> *)points {
    float maxXFov = 0, maxYFov = 0;
    for (CIVector * civector in points) {
        GLKVector3 vectorTowardsPoint = GLKVector3Negate(GLKVector3Normalize([self glkConvertPosition:civector.GLKVector3Value fromNode:nil]));
        GLKVector3 spherical = GLKVector3InSphericalCoordinates(vectorTowardsPoint);
        if (fabs(spherical.x) > maxXFov) {
            maxXFov = fabs(spherical.x);
        }
        if (fabs(spherical.y) > maxYFov) {
            maxYFov = fabs(spherical.y);
        }
    }
    self.camera.xFov = GLKMathRadiansToDegrees(maxXFov) *2 * 1.05;
    self.camera.yFov = GLKMathRadiansToDegrees(maxYFov) *2 * 1.05;
}

- (CGPoint) cameraPixelProjectionForPosition:(SCNVector3)point fromNode:(SCNNode *) sourceNode imageSize:(CGSize)imageSize{
    GLKVector3 localPoint = SCNVector3ToGLKVector3([self convertPosition: point fromNode:sourceNode]);
    GLKVector3 projectedCoord = GLKMatrix4MultiplyVector3(SCNMatrix4ToGLKMatrix4(self.camera.projectionTransform), localPoint);
    GLKVector2 normalizedPoint = GLKVector2MakeWithGLKVector3NormalizedByZ(projectedCoord);
    normalizedPoint = GLKVector2MultiplyScalar(GLKVector2AddScalar(normalizedPoint, 1.f), 0.5);
    return CGPointMakeWithGLKVector2(GLKVector2Multiply(normalizedPoint, GLKVector2MakeWithCGSize(imageSize)));
}

@end
