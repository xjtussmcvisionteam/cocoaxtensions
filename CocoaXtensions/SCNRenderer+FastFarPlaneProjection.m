//
//  SCNRenderer+FastFarPlaneProjection.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/2.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "SCNRenderer+FastFarPlaneProjection.h"

@implementation SCNRenderer (FastFarPlaneProjection)

- (SCNVector3) fastProjectPointToFarClippingPlane:(SCNVector3)point {
    static NSMutableDictionary<NSNumber *, SCNRenderer *> * rendererDictionary;
    if (!rendererDictionary) {
        rendererDictionary = [NSMutableDictionary new];
    }

    SCNRenderer * shadowRenderer  = nil;
    @synchronized (rendererDictionary) {
        shadowRenderer = rendererDictionary[@(self.hash)];
    }
    
    
    if (!shadowRenderer) {
        shadowRenderer = [SCNRenderer rendererWithDevice:MTLCreateSystemDefaultDevice() options:nil];
        shadowRenderer.pointOfView = self.pointOfView;
        CGRect viewportRect = CGRectMake(0, 0, 1.0, 1.0);
        id<MTLCommandBuffer> buffer = [shadowRenderer.commandQueue commandBuffer];
        [shadowRenderer renderAtTime:0 viewport:viewportRect commandBuffer:buffer passDescriptor:[MTLRenderPassDescriptor renderPassDescriptor]];
        [buffer commit];
        [buffer waitUntilCompleted];
        rendererDictionary[@(self.hash)] = shadowRenderer;
    }
    
    return [shadowRenderer projectPoint:point];
}

- (SCNVector3) fastProjectPointToFarClippingPlane:(SCNVector3)point canvasSize:(CGSize)canvasSize {
    SCNVector3 normalizedResult = [self fastProjectPointToFarClippingPlane:point];
    return SCNVector3Make(normalizedResult.x * canvasSize.width, normalizedResult.y * canvasSize.height, normalizedResult.z);
}

@end
