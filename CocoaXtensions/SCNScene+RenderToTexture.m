//
//  SCNScene+RenderToTexture.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/28.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "SCNScene+RenderToTexture.h"

@implementation SCNScene (RenderToTexture)

- (SCNRenderer *) cachedSCNRendererForPointOfView:(SCNNode *) nodeWithCamera {
    static NSMutableDictionary<NSNumber *, SCNRenderer *> * rendererDictionary;
    SCNRenderer * renderer = rendererDictionary[@(nodeWithCamera.hash)];
    if (!rendererDictionary) {
        rendererDictionary = [NSMutableDictionary new];
    }
    
    if (!renderer) {
        if (!nodeWithCamera.camera) {
            return nil;
        }
        
        id<MTLDevice> mtlDevice = MTLCreateSystemDefaultDevice();
        if (!mtlDevice) {
            return nil;
        }
        
        renderer = [SCNRenderer rendererWithDevice:mtlDevice options:nil];
        renderer.scene = self;
        renderer.pointOfView = nodeWithCamera;
        renderer.playing = YES;
        renderer.loops = YES;
        @synchronized (rendererDictionary) {
            rendererDictionary[@(nodeWithCamera.hash)] = renderer;
        }
    }
    return renderer;
}


- (SCNVector3) fastProjectPointToFarClippingPlane:(SCNVector3)point fromPointOfView:(SCNNode *) nodeWithCamera {
    SCNRenderer * renderer = [self cachedSCNRendererForPointOfView:nodeWithCamera];
    return [renderer fastProjectPointToFarClippingPlane:point];
}

- (SCNVector3) fastProjectPointToFarClippingPlane:(SCNVector3)point fromPointOfView:(SCNNode *) nodeWithCamera canvasSize:(CGSize)canvasSize {
    SCNVector3 normalizedResult = [self fastProjectPointToFarClippingPlane:point fromPointOfView:nodeWithCamera];
    return SCNVector3Make(normalizedResult.x * canvasSize.width, normalizedResult.y * canvasSize.height, normalizedResult.z);
}

- (id) renderToTextureFromPointOfView:(SCNNode *) nodeWithCamera withPerspectiveCorrectionForWorldQuadrilateral:(NSArray<CIVector *> *)quadrilateralPoints fullCanvasSize:(CGSize) fullCanvasSize canvasBackgroundColor:(NSUIColor *)canvasColor {
    
    SCNRenderer * renderer = [self cachedSCNRendererForPointOfView:nodeWithCamera];
    if (!renderer) {
        return nil;
    }
    
    CGPoint viewportOrigin;
    CGSize croppingTextureSize;
    CGRect smallestBoundingRect;
    SCNVector3 TLv3, TRv3, BLv3, BRv3;
    
    if (quadrilateralPoints && quadrilateralPoints.count == 4) {
        TLv3 = [self fastProjectPointToFarClippingPlane:(quadrilateralPoints[0]).SCNVector3Value fromPointOfView:nodeWithCamera];
        TRv3 = [self fastProjectPointToFarClippingPlane:(quadrilateralPoints[1]).SCNVector3Value fromPointOfView:nodeWithCamera];
        BLv3 = [self fastProjectPointToFarClippingPlane:(quadrilateralPoints[2]).SCNVector3Value fromPointOfView:nodeWithCamera];
        BRv3 = [self fastProjectPointToFarClippingPlane:(quadrilateralPoints[3]).SCNVector3Value fromPointOfView:nodeWithCamera];
        
        TLv3.y = 1.0 - TLv3.y;
        TRv3.y = 1.0 - TRv3.y;
        BLv3.y = 1.0 - BLv3.y;
        BRv3.y = 1.0 - BRv3.y;
    } else {
        TLv3 = SCNVector3Make(0, 0, 1.0);
        TRv3 = SCNVector3Make(0, 1, 1.0);
        BLv3 = SCNVector3Make(1, 0, 1.0);
        BRv3 = SCNVector3Make(1, 1, 1.0);
    }
    
    smallestBoundingRect = CGRectSmallestBoundingxBoxForCGPoints(@[[NSValue valueWithPoint:CGPointMakeWithSCNVector3(TLv3)], [NSValue valueWithPoint:CGPointMakeWithSCNVector3(TRv3)], [NSValue valueWithPoint:CGPointMakeWithSCNVector3(BLv3)], [NSValue valueWithPoint:CGPointMakeWithSCNVector3(BRv3)]]);

    if (!CGRectContainsRect(CGRectMake(0, 0, 1, 1), smallestBoundingRect)) {
        return nil;
    }
    
    croppingTextureSize = CGSizeMake(smallestBoundingRect.size.width * fullCanvasSize.width, smallestBoundingRect.size.height * fullCanvasSize.height);
    viewportOrigin = CGPointMake(- smallestBoundingRect.origin.x * fullCanvasSize.width, - smallestBoundingRect.origin.y * fullCanvasSize.height);

    // make texture
    
    static NSMutableDictionary<NSNumber *, id<MTLTexture> > * resultColorTextureDictionary;
    static NSMutableDictionary<NSNumber *, id<MTLTexture> > * resultDepthTextureDictionary;
    id<MTLTexture> resultColorTexture = resultColorTextureDictionary[@(nodeWithCamera.hash)];
    id<MTLTexture> resultDepthTexture = resultDepthTextureDictionary[@(nodeWithCamera.hash)];
    
    if (!resultColorTexture || resultColorTexture.width != croppingTextureSize.width || resultColorTexture.height != croppingTextureSize.height) {
        if (!resultColorTextureDictionary) {
            resultColorTextureDictionary = [NSMutableDictionary new];
        }
        
        // make texture
        if (resultColorTexture) {
            [resultColorTexture setPurgeableState:MTLPurgeableStateEmpty];
        }
        MTLTextureDescriptor * tdc = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatRGBA8Unorm width:croppingTextureSize.width height:croppingTextureSize.height mipmapped:NO];
        resultColorTexture = [renderer.device newTextureWithDescriptor:tdc];
        @synchronized (resultColorTextureDictionary) {
            resultColorTextureDictionary[@(nodeWithCamera.hash)] = resultColorTexture;
        }
    }
    
    if (!resultDepthTexture || resultDepthTexture.width != croppingTextureSize.width || resultDepthTexture.height != croppingTextureSize.height) {
        if (!resultDepthTextureDictionary) {
            resultDepthTextureDictionary = [NSMutableDictionary new];
        }
        
        // make texture
        if (resultDepthTexture) {
//            [resultDepthTexture setPurgeableState:MTLPurgeableStateEmpty];
        }
        MTLTextureDescriptor * tdd = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatDepth32Float width:croppingTextureSize.width height:croppingTextureSize.height mipmapped:NO];
        tdd.storageMode = MTLStorageModePrivate;
        resultDepthTexture = [renderer.device newTextureWithDescriptor:tdd];
        resultDepthTextureDictionary[@(nodeWithCamera.hash)] = resultDepthTexture;
    }
        
    // make render pass descriptor
    MTLRenderPassDescriptor * rpd = [MTLRenderPassDescriptor renderPassDescriptor];
    rpd.colorAttachments[0].texture = resultColorTexture;
    rpd.colorAttachments[0].loadAction = MTLLoadActionClear;
    rpd.colorAttachments[0].clearColor = MTLClearColorMake(canvasColor.redComponent, canvasColor.greenComponent, canvasColor.blueComponent, canvasColor.alphaComponent);
    rpd.colorAttachments[0].storeAction = MTLStoreActionStore;
    rpd.depthAttachment.texture = resultDepthTexture;
    rpd.depthAttachment.loadAction = MTLLoadActionClear;
    rpd.depthAttachment.storeAction = MTLStoreActionStore;
    rpd.depthAttachment.clearDepth = 1.f;
    
    id<MTLCommandBuffer> buffer = [renderer.commandQueue commandBuffer];
    [renderer renderAtTime:0 viewport:CGRectMakeWithCGPointAndCGSize(viewportOrigin, fullCanvasSize) commandBuffer:buffer passDescriptor:rpd];
    [buffer commit];
    [buffer waitUntilCompleted];
    
    CIImage * renderedImage = [CIImage imageWithContent:resultColorTexture];
    
    if (!quadrilateralPoints || quadrilateralPoints.count != 4) {
        return renderedImage;
    }
    
    // four corners in the cropped image
    CIVector * croppedTL = [CIVector vectorWithCGPoint:CGPointMultiplyCGSize(CGPointWithinCGRect(CGPointMakeWithSCNVector3(TLv3), smallestBoundingRect), croppingTextureSize)];
    CIVector * croppedTR = [CIVector vectorWithCGPoint:CGPointMultiplyCGSize(CGPointWithinCGRect(CGPointMakeWithSCNVector3(TRv3), smallestBoundingRect), croppingTextureSize)];
    CIVector * croppedBL = [CIVector vectorWithCGPoint:CGPointMultiplyCGSize(CGPointWithinCGRect(CGPointMakeWithSCNVector3(BLv3), smallestBoundingRect), croppingTextureSize)];
    CIVector * croppedBR = [CIVector vectorWithCGPoint:CGPointMultiplyCGSize(CGPointWithinCGRect(CGPointMakeWithSCNVector3(BRv3), smallestBoundingRect), croppingTextureSize)];
    
    CIImage * perspectiveCorrected = [renderedImage perspectiveCorrection:@[croppedTL, croppedTR, croppedBL, croppedBR]];
    
    return perspectiveCorrected;
}

- (id) renderToTextureFromPointOfView: (SCNNode *) nodeWithCamera canvasSize:(CGSize)canvasSize canvasBackgroundColor:(NSUIColor *)canvasColor {
    return [self renderToTextureFromPointOfView:nodeWithCamera withPerspectiveCorrectionForWorldQuadrilateral:nil fullCanvasSize:canvasSize canvasBackgroundColor:canvasColor];
}

@end
