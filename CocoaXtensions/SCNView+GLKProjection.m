//
//  SCNView+GLKProjection.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/10.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "SCNView+GLKProjection.h"

@implementation SCNView (GLKProjection)


- (GLKVector3) glkProjectPoint:(GLKVector3)point {
    return SCNVector3ToGLKVector3([self projectPoint:SCNVector3FromGLKVector3(point)]);
}

- (GLKVector3)glkUnprojectPoint:(GLKVector3)point {
	return SCNVector3ToGLKVector3([self unprojectPoint:SCNVector3FromGLKVector3(point)]);}

@end
