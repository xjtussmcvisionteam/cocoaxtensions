//
//  SCNHitTestResult+GLKitConversion.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/9.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <SceneKit/SceneKit.h>
#import "SCNNode+Shortcuts.h"

@interface SCNHitTestResult (GLKitConversion)

@property (nonatomic, readonly) GLKVector3 glkLocalCoordinates;
@property (nonatomic, readonly) GLKVector3 glkWorldCoordinates;
@property (nonatomic, readonly) GLKVector3 glkLocalNormal;
@property (nonatomic, readonly) GLKVector3 glkWorldNormal;
@property (nonatomic, readonly) GLKMatrix4 glkModelTransform;

@end
