//
//  SCNHitTestResult+GLKitConversion.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/9.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "SCNHitTestResult+GLKitConversion.h"

@implementation SCNHitTestResult (GLKitConversion)

@dynamic glkLocalCoordinates;
@dynamic glkWorldCoordinates;
@dynamic glkLocalNormal;
@dynamic glkWorldNormal;
@dynamic glkModelTransform;

- (GLKVector3)glkLocalCoordinates {
    return SCNVector3ToGLKVector3(self.localCoordinates);
}

- (GLKVector3)glkWorldCoordinates {
    return SCNVector3ToGLKVector3(self.worldCoordinates);
}

- (GLKVector3)glkLocalNormal {
    return SCNVector3ToGLKVector3(self.localNormal);
}

- (GLKVector3)glkWorldNormal {
    return SCNVector3ToGLKVector3(self.worldNormal);
}

- (GLKMatrix4)glkModelTransform {
    return SCNMatrix4ToGLKMatrix4(self.modelTransform);
}

@end
